From refinedc.lang Require Export notation.
From refinedc.lang Require Import tactics.
From refinedc.typing Require Import annotations.
Set Default Proof Using "Type".

(* Generated from [src/qsort.c]. *)
Section code.
  Definition file_0 : string := "src/qsort.c".
  Definition loc_2 : location_info := LocationInfo file_0 30 2 30 13.
  Definition loc_3 : location_info := LocationInfo file_0 31 2 31 17.
  Definition loc_4 : location_info := LocationInfo file_0 32 2 32 19.
  Definition loc_5 : location_info := LocationInfo file_0 36 2 53 3.
  Definition loc_6 : location_info := LocationInfo file_0 55 2 55 15.
  Definition loc_7 : location_info := LocationInfo file_0 56 2 56 11.
  Definition loc_8 : location_info := LocationInfo file_0 56 9 56 10.
  Definition loc_9 : location_info := LocationInfo file_0 56 9 56 10.
  Definition loc_10 : location_info := LocationInfo file_0 55 2 55 8.
  Definition loc_11 : location_info := LocationInfo file_0 55 2 55 8.
  Definition loc_12 : location_info := LocationInfo file_0 55 2 55 5.
  Definition loc_13 : location_info := LocationInfo file_0 55 2 55 5.
  Definition loc_14 : location_info := LocationInfo file_0 55 6 55 7.
  Definition loc_15 : location_info := LocationInfo file_0 55 6 55 7.
  Definition loc_16 : location_info := LocationInfo file_0 55 11 55 14.
  Definition loc_17 : location_info := LocationInfo file_0 55 11 55 14.
  Definition loc_18 : location_info := LocationInfo file_0 36 2 53 3.
  Definition loc_19 : location_info := LocationInfo file_0 37 2 53 3.
  Definition loc_20 : location_info := LocationInfo file_0 40 4 43 5.
  Definition loc_21 : location_info := LocationInfo file_0 44 4 44 20.
  Definition loc_22 : location_info := LocationInfo file_0 48 4 51 5.
  Definition loc_23 : location_info := LocationInfo file_0 52 4 52 20.
  Definition loc_24 : location_info := LocationInfo file_0 36 2 53 3.
  Definition loc_25 : location_info := LocationInfo file_0 36 2 53 3.
  Definition loc_26 : location_info := LocationInfo file_0 52 4 52 10.
  Definition loc_27 : location_info := LocationInfo file_0 52 4 52 10.
  Definition loc_28 : location_info := LocationInfo file_0 52 4 52 7.
  Definition loc_29 : location_info := LocationInfo file_0 52 4 52 7.
  Definition loc_30 : location_info := LocationInfo file_0 52 8 52 9.
  Definition loc_31 : location_info := LocationInfo file_0 52 8 52 9.
  Definition loc_32 : location_info := LocationInfo file_0 52 13 52 19.
  Definition loc_33 : location_info := LocationInfo file_0 52 13 52 19.
  Definition loc_34 : location_info := LocationInfo file_0 52 13 52 19.
  Definition loc_35 : location_info := LocationInfo file_0 52 13 52 16.
  Definition loc_36 : location_info := LocationInfo file_0 52 13 52 16.
  Definition loc_37 : location_info := LocationInfo file_0 52 17 52 18.
  Definition loc_38 : location_info := LocationInfo file_0 52 17 52 18.
  Definition loc_39 : location_info := LocationInfo file_0 48 4 51 5.
  Definition loc_40 : location_info := LocationInfo file_0 49 4 51 5.
  Definition loc_41 : location_info := LocationInfo file_0 50 6 50 10.
  Definition loc_42 : location_info := LocationInfo file_0 48 4 51 5.
  Definition loc_43 : location_info := LocationInfo file_0 48 4 51 5.
  Definition loc_44 : location_info := LocationInfo file_0 50 8 50 9.
  Definition loc_45 : location_info := LocationInfo file_0 50 6 50 9.
  Definition loc_46 : location_info := LocationInfo file_0 50 8 50 9.
  Definition loc_47 : location_info := LocationInfo file_0 50 8 50 9.
  Definition loc_50 : location_info := LocationInfo file_0 48 11 48 16.
  Definition loc_51 : location_info := LocationInfo file_0 48 11 48 12.
  Definition loc_52 : location_info := LocationInfo file_0 48 11 48 12.
  Definition loc_53 : location_info := LocationInfo file_0 48 15 48 16.
  Definition loc_54 : location_info := LocationInfo file_0 48 15 48 16.
  Definition loc_55 : location_info := LocationInfo file_0 48 20 48 33.
  Definition loc_56 : location_info := LocationInfo file_0 48 20 48 26.
  Definition loc_57 : location_info := LocationInfo file_0 48 20 48 26.
  Definition loc_58 : location_info := LocationInfo file_0 48 20 48 26.
  Definition loc_59 : location_info := LocationInfo file_0 48 20 48 23.
  Definition loc_60 : location_info := LocationInfo file_0 48 20 48 23.
  Definition loc_61 : location_info := LocationInfo file_0 48 24 48 25.
  Definition loc_62 : location_info := LocationInfo file_0 48 24 48 25.
  Definition loc_63 : location_info := LocationInfo file_0 48 30 48 33.
  Definition loc_64 : location_info := LocationInfo file_0 48 30 48 33.
  Definition loc_65 : location_info := LocationInfo file_0 44 4 44 10.
  Definition loc_66 : location_info := LocationInfo file_0 44 4 44 10.
  Definition loc_67 : location_info := LocationInfo file_0 44 4 44 7.
  Definition loc_68 : location_info := LocationInfo file_0 44 4 44 7.
  Definition loc_69 : location_info := LocationInfo file_0 44 8 44 9.
  Definition loc_70 : location_info := LocationInfo file_0 44 8 44 9.
  Definition loc_71 : location_info := LocationInfo file_0 44 13 44 19.
  Definition loc_72 : location_info := LocationInfo file_0 44 13 44 19.
  Definition loc_73 : location_info := LocationInfo file_0 44 13 44 19.
  Definition loc_74 : location_info := LocationInfo file_0 44 13 44 16.
  Definition loc_75 : location_info := LocationInfo file_0 44 13 44 16.
  Definition loc_76 : location_info := LocationInfo file_0 44 17 44 18.
  Definition loc_77 : location_info := LocationInfo file_0 44 17 44 18.
  Definition loc_78 : location_info := LocationInfo file_0 40 4 43 5.
  Definition loc_79 : location_info := LocationInfo file_0 41 4 43 5.
  Definition loc_80 : location_info := LocationInfo file_0 42 6 42 10.
  Definition loc_81 : location_info := LocationInfo file_0 40 4 43 5.
  Definition loc_82 : location_info := LocationInfo file_0 40 4 43 5.
  Definition loc_83 : location_info := LocationInfo file_0 42 8 42 9.
  Definition loc_84 : location_info := LocationInfo file_0 42 6 42 9.
  Definition loc_85 : location_info := LocationInfo file_0 42 8 42 9.
  Definition loc_86 : location_info := LocationInfo file_0 42 8 42 9.
  Definition loc_89 : location_info := LocationInfo file_0 40 11 40 16.
  Definition loc_90 : location_info := LocationInfo file_0 40 11 40 12.
  Definition loc_91 : location_info := LocationInfo file_0 40 11 40 12.
  Definition loc_92 : location_info := LocationInfo file_0 40 15 40 16.
  Definition loc_93 : location_info := LocationInfo file_0 40 15 40 16.
  Definition loc_94 : location_info := LocationInfo file_0 40 20 40 33.
  Definition loc_95 : location_info := LocationInfo file_0 40 20 40 26.
  Definition loc_96 : location_info := LocationInfo file_0 40 20 40 26.
  Definition loc_97 : location_info := LocationInfo file_0 40 20 40 26.
  Definition loc_98 : location_info := LocationInfo file_0 40 20 40 23.
  Definition loc_99 : location_info := LocationInfo file_0 40 20 40 23.
  Definition loc_100 : location_info := LocationInfo file_0 40 24 40 25.
  Definition loc_101 : location_info := LocationInfo file_0 40 24 40 25.
  Definition loc_102 : location_info := LocationInfo file_0 40 30 40 33.
  Definition loc_103 : location_info := LocationInfo file_0 40 30 40 33.
  Definition loc_104 : location_info := LocationInfo file_0 36 9 36 14.
  Definition loc_105 : location_info := LocationInfo file_0 36 9 36 10.
  Definition loc_106 : location_info := LocationInfo file_0 36 9 36 10.
  Definition loc_107 : location_info := LocationInfo file_0 36 13 36 14.
  Definition loc_108 : location_info := LocationInfo file_0 36 13 36 14.
  Definition loc_109 : location_info := LocationInfo file_0 32 12 32 18.
  Definition loc_110 : location_info := LocationInfo file_0 32 12 32 18.
  Definition loc_111 : location_info := LocationInfo file_0 32 12 32 18.
  Definition loc_112 : location_info := LocationInfo file_0 32 12 32 15.
  Definition loc_113 : location_info := LocationInfo file_0 32 12 32 15.
  Definition loc_114 : location_info := LocationInfo file_0 32 16 32 17.
  Definition loc_115 : location_info := LocationInfo file_0 32 16 32 17.
  Definition loc_118 : location_info := LocationInfo file_0 31 10 31 16.
  Definition loc_119 : location_info := LocationInfo file_0 31 10 31 12.
  Definition loc_120 : location_info := LocationInfo file_0 31 10 31 12.
  Definition loc_121 : location_info := LocationInfo file_0 31 15 31 16.
  Definition loc_124 : location_info := LocationInfo file_0 30 10 30 12.
  Definition loc_125 : location_info := LocationInfo file_0 30 10 30 12.
  Definition loc_130 : location_info := LocationInfo file_0 74 2 79 3.
  Definition loc_131 : location_info := LocationInfo file_0 75 2 79 3.
  Definition loc_132 : location_info := LocationInfo file_0 76 4 76 35.
  Definition loc_133 : location_info := LocationInfo file_0 77 4 77 22.
  Definition loc_134 : location_info := LocationInfo file_0 78 4 78 22.
  Definition loc_135 : location_info := LocationInfo file_0 78 4 78 9.
  Definition loc_136 : location_info := LocationInfo file_0 78 4 78 9.
  Definition loc_137 : location_info := LocationInfo file_0 78 10 78 13.
  Definition loc_138 : location_info := LocationInfo file_0 78 10 78 13.
  Definition loc_139 : location_info := LocationInfo file_0 78 15 78 16.
  Definition loc_140 : location_info := LocationInfo file_0 78 15 78 16.
  Definition loc_141 : location_info := LocationInfo file_0 78 18 78 20.
  Definition loc_142 : location_info := LocationInfo file_0 78 18 78 20.
  Definition loc_143 : location_info := LocationInfo file_0 77 4 77 9.
  Definition loc_144 : location_info := LocationInfo file_0 77 4 77 9.
  Definition loc_145 : location_info := LocationInfo file_0 77 10 77 13.
  Definition loc_146 : location_info := LocationInfo file_0 77 10 77 13.
  Definition loc_147 : location_info := LocationInfo file_0 77 15 77 17.
  Definition loc_148 : location_info := LocationInfo file_0 77 15 77 17.
  Definition loc_149 : location_info := LocationInfo file_0 77 19 77 20.
  Definition loc_150 : location_info := LocationInfo file_0 77 19 77 20.
  Definition loc_151 : location_info := LocationInfo file_0 76 12 76 34.
  Definition loc_152 : location_info := LocationInfo file_0 76 12 76 21.
  Definition loc_153 : location_info := LocationInfo file_0 76 12 76 21.
  Definition loc_154 : location_info := LocationInfo file_0 76 22 76 25.
  Definition loc_155 : location_info := LocationInfo file_0 76 22 76 25.
  Definition loc_156 : location_info := LocationInfo file_0 76 27 76 29.
  Definition loc_157 : location_info := LocationInfo file_0 76 27 76 29.
  Definition loc_158 : location_info := LocationInfo file_0 76 31 76 33.
  Definition loc_159 : location_info := LocationInfo file_0 76 31 76 33.
  Definition loc_163 : location_info := LocationInfo file_0 74 6 74 13.
  Definition loc_164 : location_info := LocationInfo file_0 74 6 74 8.
  Definition loc_165 : location_info := LocationInfo file_0 74 6 74 8.
  Definition loc_166 : location_info := LocationInfo file_0 74 11 74 13.
  Definition loc_167 : location_info := LocationInfo file_0 74 11 74 13.

  (* Definition of function [partition]. *)
  Definition impl_partition : function := {|
    f_args := [
      ("arr", void*);
      ("lo", it_layout i32);
      ("hi", it_layout i32)
    ];
    f_local_vars := [
      ("i", it_layout i32);
      ("key", it_layout i32);
      ("j", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "i" <-{ it_layout i32 }
          LocInfoE loc_124 (use{it_layout i32} (LocInfoE loc_125 ("lo"))) ;
        "j" <-{ it_layout i32 }
          LocInfoE loc_118 ((LocInfoE loc_119 (use{it_layout i32} (LocInfoE loc_120 ("hi")))) -{IntOp i32, IntOp i32} (LocInfoE loc_121 (i2v 1 i32))) ;
        "key" <-{ it_layout i32 }
          LocInfoE loc_109 (use{it_layout i32} (LocInfoE loc_111 ((LocInfoE loc_112 (!{void*} (LocInfoE loc_113 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_114 (use{it_layout i32} (LocInfoE loc_115 ("i"))))))) ;
        locinfo: loc_5 ;
        Goto "#1"
      ]> $
      <[ "#1" :=
        locinfo: loc_104 ;
        if: LocInfoE loc_104 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_104 ((LocInfoE loc_105 (use{it_layout i32} (LocInfoE loc_106 ("i")))) <{IntOp i32, IntOp i32} (LocInfoE loc_107 (use{it_layout i32} (LocInfoE loc_108 ("j")))))))
        then
        locinfo: loc_20 ;
          Goto "#2"
        else
        locinfo: loc_6 ;
          Goto "#3"
      ]> $
      <[ "#10" :=
        locinfo: loc_55 ;
        if: LocInfoE loc_55 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_55 ((LocInfoE loc_56 (use{it_layout i32} (LocInfoE loc_58 ((LocInfoE loc_59 (!{void*} (LocInfoE loc_60 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_61 (use{it_layout i32} (LocInfoE loc_62 ("i")))))))) ≤{IntOp i32, IntOp i32} (LocInfoE loc_63 (use{it_layout i32} (LocInfoE loc_64 ("key")))))))
        then
        locinfo: loc_41 ;
          Goto "#8"
        else
        locinfo: loc_23 ;
          Goto "#9"
      ]> $
      <[ "#11" :=
        locinfo: loc_94 ;
        if: LocInfoE loc_94 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_94 ((LocInfoE loc_95 (use{it_layout i32} (LocInfoE loc_97 ((LocInfoE loc_98 (!{void*} (LocInfoE loc_99 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_100 (use{it_layout i32} (LocInfoE loc_101 ("j")))))))) ≥{IntOp i32, IntOp i32} (LocInfoE loc_102 (use{it_layout i32} (LocInfoE loc_103 ("key")))))))
        then
        locinfo: loc_80 ;
          Goto "#5"
        else
        locinfo: loc_21 ;
          Goto "#6"
      ]> $
      <[ "#2" :=
        locinfo: loc_20 ;
        Goto "#4"
      ]> $
      <[ "#3" :=
        locinfo: loc_6 ;
        LocInfoE loc_11 ((LocInfoE loc_12 (!{void*} (LocInfoE loc_13 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_14 (use{it_layout i32} (LocInfoE loc_15 ("i"))))) <-{ it_layout i32 }
          LocInfoE loc_16 (use{it_layout i32} (LocInfoE loc_17 ("key"))) ;
        locinfo: loc_7 ;
        Return (LocInfoE loc_8 (use{it_layout i32} (LocInfoE loc_9 ("i"))))
      ]> $
      <[ "#4" :=
        locinfo: loc_89 ;
        if: LocInfoE loc_89 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_89 ((LocInfoE loc_90 (use{it_layout i32} (LocInfoE loc_91 ("i")))) <{IntOp i32, IntOp i32} (LocInfoE loc_92 (use{it_layout i32} (LocInfoE loc_93 ("j")))))))
        then
        Goto "#11"
        else
        locinfo: loc_21 ;
          Goto "#6"
      ]> $
      <[ "#5" :=
        locinfo: loc_80 ;
        LocInfoE loc_83 ("j") <-{ it_layout i32 }
          LocInfoE loc_84 ((LocInfoE loc_85 (use{it_layout i32} (LocInfoE loc_86 ("j")))) -{IntOp i32, IntOp i32} (i2v 1 i32)) ;
        locinfo: loc_81 ;
        Goto "continue4"
      ]> $
      <[ "#6" :=
        locinfo: loc_21 ;
        LocInfoE loc_66 ((LocInfoE loc_67 (!{void*} (LocInfoE loc_68 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_69 (use{it_layout i32} (LocInfoE loc_70 ("i"))))) <-{ it_layout i32 }
          LocInfoE loc_71 (use{it_layout i32} (LocInfoE loc_73 ((LocInfoE loc_74 (!{void*} (LocInfoE loc_75 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_76 (use{it_layout i32} (LocInfoE loc_77 ("j"))))))) ;
        locinfo: loc_22 ;
        Goto "#7"
      ]> $
      <[ "#7" :=
        locinfo: loc_50 ;
        if: LocInfoE loc_50 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_50 ((LocInfoE loc_51 (use{it_layout i32} (LocInfoE loc_52 ("i")))) <{IntOp i32, IntOp i32} (LocInfoE loc_53 (use{it_layout i32} (LocInfoE loc_54 ("j")))))))
        then
        Goto "#10"
        else
        locinfo: loc_23 ;
          Goto "#9"
      ]> $
      <[ "#8" :=
        locinfo: loc_41 ;
        LocInfoE loc_44 ("i") <-{ it_layout i32 }
          LocInfoE loc_45 ((LocInfoE loc_46 (use{it_layout i32} (LocInfoE loc_47 ("i")))) +{IntOp i32, IntOp i32} (i2v 1 i32)) ;
        locinfo: loc_42 ;
        Goto "continue6"
      ]> $
      <[ "#9" :=
        locinfo: loc_23 ;
        LocInfoE loc_27 ((LocInfoE loc_28 (!{void*} (LocInfoE loc_29 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_30 (use{it_layout i32} (LocInfoE loc_31 ("j"))))) <-{ it_layout i32 }
          LocInfoE loc_32 (use{it_layout i32} (LocInfoE loc_34 ((LocInfoE loc_35 (!{void*} (LocInfoE loc_36 ("arr")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_37 (use{it_layout i32} (LocInfoE loc_38 ("i"))))))) ;
        locinfo: loc_24 ;
        Goto "continue2"
      ]> $
      <[ "continue2" :=
        locinfo: loc_5 ;
        Goto "#1"
      ]> $
      <[ "continue4" :=
        locinfo: loc_20 ;
        Goto "#4"
      ]> $
      <[ "continue6" :=
        locinfo: loc_22 ;
        Goto "#7"
      ]> $∅
    )%E
  |}.

  (* Definition of function [qsort]. *)
  Definition impl_qsort (global_partition global_qsort : loc): function := {|
    f_args := [
      ("arr", void*);
      ("lo", it_layout i32);
      ("hi", it_layout i32)
    ];
    f_local_vars := [
      ("k", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_163 ;
        if: LocInfoE loc_163 (UnOp (CastOp $ IntOp bool_it) (IntOp i32) (LocInfoE loc_163 ((LocInfoE loc_164 (use{it_layout i32} (LocInfoE loc_165 ("lo")))) <{IntOp i32, IntOp i32} (LocInfoE loc_166 (use{it_layout i32} (LocInfoE loc_167 ("hi")))))))
        then
        Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        "k" <-{ it_layout i32 }
          LocInfoE loc_151 (Call (LocInfoE loc_153 (global_partition)) [@{expr} LocInfoE loc_154 (use{void*} (LocInfoE loc_155 ("arr"))) ;
          LocInfoE loc_156 (use{it_layout i32} (LocInfoE loc_157 ("lo"))) ;
          LocInfoE loc_158 (use{it_layout i32} (LocInfoE loc_159 ("hi"))) ]) ;
        locinfo: loc_133 ;
        expr: (LocInfoE loc_133 (Call (LocInfoE loc_144 (global_qsort)) [@{expr} LocInfoE loc_145 (use{void*} (LocInfoE loc_146 ("arr"))) ;
        LocInfoE loc_147 (use{it_layout i32} (LocInfoE loc_148 ("lo"))) ;
        LocInfoE loc_149 (use{it_layout i32} (LocInfoE loc_150 ("k"))) ])) ;
        locinfo: loc_134 ;
        expr: (LocInfoE loc_134 (Call (LocInfoE loc_136 (global_qsort)) [@{expr} LocInfoE loc_137 (use{void*} (LocInfoE loc_138 ("arr"))) ;
        LocInfoE loc_139 (use{it_layout i32} (LocInfoE loc_140 ("k"))) ;
        LocInfoE loc_141 (use{it_layout i32} (LocInfoE loc_142 ("hi"))) ])) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        Return (VOID)
      ]> $∅
    )%E
  |}.
End code.
