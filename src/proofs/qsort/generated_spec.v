From refinedc.typing Require Import typing.
From refinedc.project.quick_sort.src.qsort Require Import generated_code.
From refinedc.project.quick_sort.src.qsort Require Import qsort.
Set Default Proof Using "Type".

(* Generated from [src/qsort.c]. *)
Section spec.
  Context `{!typeG Σ} `{!globalG Σ}.

  (* Type definitions. *)

  (* Specifications for function [partition]. *)
  Definition type_of_partition :=
    fn(∀ (a, es, lo, hi) : loc * (list Z) * nat * nat; (a @ (&own (array (i32) (es `at_type` (int i32))))), (lo @ (int (i32))), (hi @ (int (i32))); ⌜lo < hi <= length es⌝)
      → ∃ (index, pivot, xs) : nat * Z * (list Z), (index @ (int (i32))); ⌜lo <= index < hi⌝ ∗ (a ◁ₗ (array (i32) (xs `at_type` (int i32)))) ∗ ⌜unchanged lo hi xs es⌝ ∗ ⌜length xs = length es⌝ ∗ ⌜Permutation xs es⌝ ∗ ⌜xs !! index = Some pivot⌝ ∗ ⌜partitioned lo index index hi pivot xs⌝.

  (* Specifications for function [qsort]. *)
  Definition type_of_qsort :=
    fn(∀ (a, es, lo, hi) : loc * (list Z) * nat * nat; (a @ (&own (array (i32) (es `at_type` (int i32))))), (lo @ (int (i32))), (hi @ (int (i32))); ⌜hi <= length es <= max_int i32⌝)
      → ∃ xs : (list Z), (void); (a ◁ₗ (array (i32) (xs `at_type` (int i32)))) ∗ ⌜unchanged lo hi xs es⌝ ∗ ⌜length xs = length es⌝ ∗ ⌜Permutation xs es⌝ ∗ ⌜sorted lo hi xs⌝.
End spec.
