From refinedc.typing Require Import typing.

(** Predicates **)

Definition unchanged (l r : nat) {A : Type} (xs ys : list A) :=
  (∀ i, (i < l)%nat → (xs !! i = ys !! i)) ∧
  (∀ i, (r ≤ i)%nat → (xs !! i = ys !! i)).

Definition partitioned (l s t r : nat) (key : Z) (xs : list Z) :=
  (∀ i x, (l ≤ i < s)%nat → xs !! i = Some x → x ≤ key) ∧
  (∀ i x, (t < i < r)%nat → xs !! i = Some x → key ≤ x).

Definition sorted (l r : nat) (xs : list Z) :=
  ∀ i j x y, (l ≤ i ∧ i < j ∧ j < r)%nat →
    xs !! i = Some x → xs !! j = Some y → x ≤ y.

(** Solvers for predicates **)

Ltac subst_insert_id :=
  match goal with
  | [ H : ?xs !! ?i = Some ?x |- context [ (<[?i:=?x]> ?xs) ] ] =>
    rewrite (list_insert_id _ _ _ H)
  end.

Ltac split_eq i j := destruct (decide (i = j)) as [<-|].

Ltac case_split_insert :=
  match goal with
  | |- context [ <[?j:=?y]> ?xs !! ?i ] =>
    split_eq i j;
    [ rewrite list_lookup_insert;
      [ first [ solve_goal | simplify_eq ] | done ]
    | rewrite list_lookup_insert_ne; last done ]
  | [ H : <[?j:=?y]> ?xs !! ?i = Some ?x |- _ ] =>
    split_eq i j;
    [ rewrite list_lookup_insert in H;
      [ first [ solve_goal | simplify_eq ] | done ]
    | rewrite list_lookup_insert_ne in H; last done ]
  end.

Ltac solve_unchanged :=
  match goal with
  | |- ?xs !! ?i = ?xs !! ?i => done
  | [ H : ∀ I, _ → ?xs !! I = _ !! I |- ?xs !! ?i = _ !! ?i ] =>
    rewrite (H i); [ solve_unchanged | solve_goal ]
  end.

Ltac solve_partitioned :=
  match goal with
  | [ H : ?x ≤ ?y |- ?x ≤ ?y ] => apply H
  | [ _ : ?y >= ?x |- ?x ≤ ?y ] => by apply Z.ge_le
  | [ HX : ?xs !! ?i = Some ?x, H : ∀ I X, _ → ?xs !! I = Some X → X ≤ ?key
    |- ?x ≤ ?key ] => apply (H i x); [ solve_goal | apply HX ]
  | [ HX : ?xs !! ?i = Some ?x, H : ∀ I X, _ → ?xs !! I = Some X → ?key ≤ X
    |- ?key ≤ ?x ] => apply (H i x); [ solve_goal | apply HX ]
  end.

(** Solvers for permutations **)

Lemma perm_insert_swap {A : Type} (l1 l2 : list A) i j x y :
  (i < length l1)%nat →
  l1 !! j = Some y →
  <[i:=x]> l1 ≡ₚ l2 →
  <[j:=x]> (<[i:=y]> l1) ≡ₚ l2.
Proof.
  split_eq i j; first by intros; subst_insert_id.
  move => Hlt Hj /Permutation_inj [Hlen [f [Hinj Heq]]].
  (* i ≠ j *)
  rewrite insert_length in Hlen.
  apply Permutation_inj. split.
  - by rewrite -Hlen insert_length insert_length.
  - exists (λ k, if bool_decide (k = i) then f j
      else if bool_decide (k = j) then f i else f k).
    split.
    + rewrite /Inj => a b Hab.
      repeat case_bool_decide; apply Hinj in Hab; congruence.
    + move => k.
      have ? : (j < length (<[i:=y]> l1))%nat.
      { rewrite insert_length. apply lookup_lt_is_Some. by exists y. }
      repeat case_bool_decide; simplify_eq; rewrite -Heq.
      all: by do!
        [ rewrite list_lookup_insert_ne; last solve_goal
        | rewrite list_lookup_insert;    last solve_goal ].
Qed.

Ltac solve_Permutation :=
  match goal with
  | |- ?xs ≡ₚ ?xs => done
  | [ H : ?xs ≡ₚ _ |- ?xs ≡ₚ _ ] => rewrite H; solve_Permutation
  | [ Hy : ?xs !! ?j = Some ?y, Hp : <[?i:=?x]> ?xs ≡ₚ ?ys 
    |- <[?j:=?x]> (<[?i:=?y]> ?xs) ≡ₚ ?ys ] =>
    apply perm_insert_swap; [ done | apply Hy | apply Hp ]
  end.

(** Properties for sorted **)

(* Definition of `range` and its properties *)

Definition range {A : Type} (l : list A) (a b : nat) := drop a (take b l).

Notation "l .[ a , b )" := (range l a b) (at level 5, format "l .[ a ,  b )").

Notation "l .[ , b )" := (range l 0 b) (at level 5, only parsing).

Notation "l .[ a , )" := (range l a (length l)) (at level 5, only parsing).

Lemma range_empty a b {A : Type} (l : list A) :
  (b ≤ a)%nat → l.[a, b) = [].
Proof.
  move => ?.
  apply drop_ge.
  rewrite take_length. lia.
Qed.

Lemma range_full {A : Type} (l : list A) :
  l.[0, ) = l.
Proof.
  by rewrite /range drop_0 firstn_all.
Qed.

Lemma range_length a b {A : Type} (l : list A) :
  length l.[a, b) = (b `min` length l - a)%nat.
Proof.
  by rewrite drop_length take_length.
Qed.

Lemma range_cons a b {A : Type} (l : list A) x :
  (a < b)%nat → l !! a = Some x → l.[a, b) = x :: l.[a + 1, b).
Proof.
  move => ? ?.
  rewrite /range. 
  have -> : (a + 1)%nat = S a by lia.
  apply drop_S.
  by rewrite lookup_take.
Qed.

Lemma range_split k a b {A : Type} (l : list A) :
  (a ≤ k ≤ b)%nat → l.[a, b) = l.[a, k) ++ l.[k, b).
Proof.
  move => [? ?].
  rewrite /range.
  have -> : take k l = take k (take b l) by rewrite take_take min_l.
  by rewrite drop_take_drop.
Qed.

Lemma range_lookup_Some {A : Type} (l : list A) a b i x :
  l.[a, b) !! i = Some x ↔ (a + i < b)%nat ∧ l !! (a + i)%nat = Some x.
Proof.
  rewrite lookup_drop. split.
  - destruct (decide (a + i < b)%nat).
    + by rewrite lookup_take.
    + by rewrite lookup_take_ge; [ | lia ].
  - move => [? ?].
    by rewrite lookup_take.
Qed.

Lemma range_forall {A : Type} (l : list A) (a b : nat) P 
  `{∀ x, Decision (P x)} :
  (∀ i x, l.[a, b) !! i = Some x → P x) ↔
    (∀ i x, (a ≤ i < b)%nat → l !! i = Some x → P x).
Proof.
  setoid_rewrite range_lookup_Some.
  split.
  - move => He i x [? ?] ?.
    apply (He (i - a)%nat); split.
    all: by have -> : (a + (i - a))%nat = i by lia.
  - move => He i x [? ?].
    by apply (He (a + i)%nat); [ lia | ].
Qed.

Lemma range_forall2 {A : Type} (l1 l2 : list A) (a b : nat) P 
  `{∀ x y, Decision (P x y)} :
  (∀ i x y, l1.[a, b) !! i = Some x → l2.[a, b) !! i = Some y → P x y) ↔
    (∀ i x y, (a ≤ i < b)%nat → l1 !! i = Some x → l2 !! i = Some y → P x y).
Proof.
  setoid_rewrite range_lookup_Some.
  split.
  - move => He i x y [? ?] ? ?.
    apply (He (i - a)%nat); split.
    all: by have -> : (a + (i - a))%nat = i by lia.
  - move => He i x y [? ?] [? ?].
    apply (He (a + i)%nat); by [ lia | ].
Qed.

Lemma range_forall_I {A : Type} (l : list A) (a b : nat) I P 
  `{∀ x y, Decision (I x y)} `{∀ x y, Decision (P x y)} :
  (∀ i j, I i j ↔ I (a + i)%nat (a + j)%nat) →
  (∀ i j x y, I i j → l.[a, b) !! i = Some x → l.[a, b) !! j = Some y → P x y) ↔
    (∀ i j x y, (a ≤ i < b ∧ a ≤ j < b ∧ I i j)%nat →
      l !! i = Some x → l !! j = Some y → P x y).
Proof.
  move => HI.
  setoid_rewrite range_lookup_Some.
  split.
  - move => He i j x y [[? ?] [[? ?] ?]] ? ?.
    apply (He (i - a)%nat (j - a)%nat); [ rewrite HI | split.. ].
    all: try have -> : (a + (i - a))%nat = i by lia.
    all: by try have -> : (a + (j - a))%nat = j by lia.
  - move => He i j x y ? [? ?] [? ?].
    apply (He (a + i)%nat (a + j)%nat); first rewrite -HI; repeat split.
    all: by try lia.
Qed.

(* Helper lemmas: range equality *)

Lemma list_partial_eq_same_length I {A : Type} (l1 l2 : list A) :
  length l1 = length l2 →
    (∀ i, I i → l1 !! i = l2 !! i) ↔
      (∀ i x y, I i → l1 !! i = Some x → l2 !! i = Some y → x = y).
Proof.
  move => Hlen. split.
  - move => He i x y ? ? ?.
    have : l1 !! i = l2 !! i by apply (He i). congruence.
  - move => He i Hi.
    rewrite option_eq => x. split.
    + move => Hx.
      have [y ?] : is_Some (l2 !! i).
      { rewrite lookup_lt_is_Some -Hlen. by eapply lookup_lt_Some. }
      have : x = y by apply (He i x y). congruence.
    + move => Hx.
      have [y Hy] : is_Some (l1 !! i).
      { rewrite lookup_lt_is_Some Hlen. by eapply lookup_lt_Some. }
      have : y = x by apply (He i y x). congruence.
Qed.

Lemma list_eq_iff {A : Type} (l1 l2 : list A) :
  l1 = l2 ↔ (∀ i, l1 !! i = l2 !! i).
Proof.
  split; by [ move => -> | apply list_eq ].
Qed.

Lemma range_eq_same_length a b {A : Type} `{∀ (x y : A), Decision (x = y)}
  (l1 l2 : list A) :
  length l1 = length l2 →
    l1.[a, b) = l2.[a, b) ↔ (∀ i, (a ≤ i < b)%nat → l1 !! i = l2 !! i).
Proof.
  move => Hlen.
  rewrite list_partial_eq_same_length -?range_forall2 => //.
  have HP : ∀ P, P ↔ (True → P) by intuition.
  setoid_rewrite HP at 6.
  rewrite -list_partial_eq_same_length ?list_eq_iff ?range_length ?Hlen => //.
  intuition.
Qed.

(* Helper lemmas: partitioned *)

Lemma partitioned_iff_Forall (l s t r : nat) (key : Z) (xs : list Z) :
  partitioned l s t r key xs ↔
    Forall (λ x, x ≤ key) xs.[l, s) ∧ Forall (λ x, key ≤ x) xs.[t + 1, r).
Proof.
  rewrite /partitioned !Forall_lookup.
  have Ht : ∀ i, (t < i ↔ t + 1 ≤ i)%nat by lia.
  setoid_rewrite Ht.
  rewrite -!range_forall. intuition.
Qed.

(* Helper lemmas: sorted *)

Lemma StronglySorted_forall {A : Type} (l : list A) R :
  StronglySorted R l ↔ 
    (∀ i j x y, (i < j)%nat → l !! i = Some x → l !! j = Some y → R x y).
Proof.
  split.
  - induction 1 as [|a l ? IH HR] => //.
    move => i j x y ? ? Hy.
    have ? : (a :: l) !! j = l !! pred j
      by rewrite lookup_cons_ne_0; last lia.
    destruct i.
    + (* i = 0 *)
      have -> : x = a by naive_solver.
      move: HR => /Forall_lookup HR.
      apply (HR (pred j) y). by rewrite -Hy.
    + (* i > 0 *)
      apply (IH i (pred j)); [ lia | naive_solver | by rewrite -Hy ].
  - induction l as [|a l IH]; first constructor.
    move => HR. constructor.
    + apply IH. move => i j x y ? ? ?.
      apply (HR (S i) (S j)); [ lia | naive_solver.. ].
    + rewrite Forall_lookup => i x Hi.
      apply (HR O (S i)); [ lia | naive_solver.. ].
Qed.

Lemma sorted_iff_StronglySorted l r xs :
  sorted l r xs ↔ StronglySorted Z.le xs.[l, r).
Proof.
  rewrite /sorted StronglySorted_forall.
  have Hij : ∀ i j,
    (l ≤ i ∧ i < j ∧ j < r ↔ l ≤ i < r ∧ l ≤ j < r ∧ i < j)%nat by lia.
  setoid_rewrite Hij.
  by rewrite -range_forall_I; [ | lia ].
Qed.

Lemma StronglySorted_app {A : Type} (l1 l2 : list A) R x :
  Transitive R →
  StronglySorted R l1 →
  StronglySorted R l2 →
  Forall (λ e, R e x) l1 →
  Forall (R x) l2 →
  StronglySorted R (l1 ++ l2).
Proof.
  induction 2 as [|a l ? IH ?] => //.
  move => ? /Forall_cons [? ?] ?.
  rewrite -app_comm_cons.
  constructor.
  - by apply IH.
  - apply Forall_app. split => //.
    eapply Forall_impl => //. by etrans.
Qed.

(* Main theorems *)

Lemma sorted_empty xs lo hi :
  (hi ≤ lo)%nat → sorted lo hi xs.
Proof.
  rewrite sorted_iff_StronglySorted => /range_empty ->.
  constructor.
Qed.

Ltac solve_range_eq :=
  match goal with
  | |- _.[?l, ?r) = _.[?l, ?r) =>
    rewrite range_eq_same_length; [ intros; solve_unchanged | done ]
  | [ H : length ?xs = length ?ys |- ?xs.[?l, ) = ?ys.[?l, ) ] =>
    rewrite H; solve_range_eq
  end.

Lemma sorted_combine (xs : list Z) xs1 xs2 xs3 k key lo hi :
  (lo ≤ k < hi)%nat →
  (hi ≤ length xs) →
  length xs1 = length xs →
  xs1 !! k = Some key →
  partitioned lo k k hi key xs1 →
  xs2 ≡ₚ xs1 →
  unchanged lo k xs2 xs1 →
  sorted lo k xs2 →
  xs3 ≡ₚ xs2 →
  unchanged k hi xs3 xs2 →
  sorted k hi xs3 →
  sorted lo hi xs3.
Proof.
  move => [? ?] ? ? Hkey /partitioned_iff_Forall [Hle Hge] ? [? ?]
       => /sorted_iff_StronglySorted Hsl ? [? ?]
       => /sorted_iff_StronglySorted Hsr.
  have ? : length xs2 = length xs1 by apply Permutation_length.
  have ? : length xs3 = length xs2 by apply Permutation_length.
  rewrite sorted_iff_StronglySorted (range_split k); last lia.
  eapply StronglySorted_app.
  - rewrite /Transitive. by apply Z.le_trans.
  - have -> : xs3.[lo, k) = xs2.[lo, k) by solve_range_eq.
    by apply Hsl.
  - by apply Hsr.
  - have -> : xs3.[lo, k) = xs2.[lo, k) by solve_range_eq.
    eapply Forall_Permutation; last by apply Hle.
    apply Permutation_app_inv_l with (l := xs2.[, lo)).
    have {2}-> : xs2.[, lo) = xs1.[, lo) by solve_range_eq.
    apply Permutation_app_inv_r with (l := xs2.[k, )).
    have {2}-> : xs2.[k, ) = xs1.[k, ) by solve_range_eq.
    rewrite -!range_split ?range_full => //; lia.
  - have : Forall (λ x : Z, key ≤ x) xs1.[k, hi).
    { split_eq k hi.
      + rewrite range_empty => //; apply Forall_nil.
      + erewrite range_cons; by [ apply Forall_cons | lia | apply Hkey ]. }
    have : xs2.[k, hi) = xs1.[k, hi) by solve_range_eq.
    move => <- Hge'.
    eapply Forall_Permutation; last by apply Hge'.
    apply Permutation_app_inv_l with (l := xs3.[, k)).
    have {2}-> : xs3.[, k) = xs2.[, k) by solve_range_eq.
    apply Permutation_app_inv_r with (l := xs3.[hi, )).
    have {2}-> : xs3.[hi, ) = xs2.[hi, ) by solve_range_eq.
    rewrite -!range_split ?range_full => //; lia.
Qed.