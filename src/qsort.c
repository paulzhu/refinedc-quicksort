//@rc::import qsort from refinedc.project.quick_sort.src.qsort

#define LOOP_INVS \
  [[rc::exists("i : nat", "j : nat", "key : Z", "ys : {list Z}")]] \
  [[rc::inv_vars("i : i @ int<i32>", "j : j @ int<i32>", "key : key @ int<i32>", \
                  "arr : a @ &own<array<i32, {ys `at_type` (int i32)}>>")]] \
  [[rc::constraints("{lo <= i}", "{i <= j}", "{j < hi}", "{length ys = length es}")]] \
  [[rc::constraints("{unchanged lo hi ys es}")]] \
  [[rc::constraints("{partitioned lo i j hi key ys}")]]

/* Helper function: split `arr[lo..hi)` into two parts: <= pivot and >= pivot */
[[rc::parameters("a : loc", "es : {list Z}", "lo : nat", "hi : nat")]]
[[rc::args("a @ &own<array<i32, {es `at_type` (int i32)}>>", "lo @ int<i32>", "hi @ int<i32>")]]
[[rc::requires("{lo < hi <= length es}")]]
[[rc::exists("index : nat", "pivot : Z", "xs : {list Z}")]]
[[rc::returns("index @ int<i32>")]]
[[rc::ensures("{lo <= index < hi}")]]
[[rc::ensures("own a : array<i32, {xs `at_type` (int i32)}>")]]
[[rc::ensures("{unchanged lo hi xs es}")]]
[[rc::ensures("{length xs = length es}", "{Permutation xs es}")]]
[[rc::ensures("{xs !! index = Some pivot}")]]
[[rc::ensures("{partitioned lo index index hi pivot xs}")]]
[[rc::tactics("all: repeat subst_insert_id.")]]
[[rc::tactics("all: try case_split_insert.")]]
[[rc::tactics("all: try solve_unchanged.")]]
[[rc::tactics("all: try solve_partitioned.")]]
[[rc::tactics("all: try solve_Permutation.")]]
int partition(int* arr, int lo, int hi)
{
  int i = lo;
  int j = hi - 1; // `hi` is exclusive
  int key = arr[i];
  
  LOOP_INVS
  [[rc::constraints("{Permutation (<[i:=key]> ys) es}")]]
  while (i < j)
  {
    LOOP_INVS
    [[rc::constraints("{Permutation (<[i:=key]> ys) es}")]]
    while (i < j && arr[j] >= key)
    {
      --j;
    }
    arr[i] = arr[j];
    
    LOOP_INVS
    [[rc::constraints("{Permutation (<[j:=key]> ys) es}")]]
    while (i < j && arr[i] <= key)
    {
      ++i;
    }
    arr[j] = arr[i];
  }

  arr[i] = key;
  return i;
}

/* Quick sort: sorting `arr[lo..hi)` */
[[rc::parameters("a : loc", "es : {list Z}", "lo : nat", "hi : nat")]]
[[rc::args("a @ &own<array<i32, {es `at_type` (int i32)}>>", "lo @ int<i32>", "hi @ int<i32>")]]
[[rc::requires("{hi <= length es <= max_int i32}")]]
[[rc::exists("xs : {list Z}")]]
[[rc::ensures("own a : array<i32, {xs `at_type` (int i32)}>")]]
[[rc::ensures("{unchanged lo hi xs es}")]]
[[rc::ensures("{length xs = length es}", "{Permutation xs es}")]]
[[rc::ensures("{sorted lo hi xs}")]]
[[rc::tactics("all: try solve_unchanged.")]]
[[rc::tactics("all: try solve_Permutation.")]]
[[rc::lemmas("sorted_empty")]]
[[rc::tactics("all: try by apply: sorted_combine; try done; solve_goal.")]]
void qsort(int* arr, int lo, int hi)
{
  if (lo < hi)
  {
    int k = partition(arr, lo, hi);
    qsort(arr, lo, k);
    qsort(arr, k, hi);
  }
}
